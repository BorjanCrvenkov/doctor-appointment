<?php

namespace Service;

use Models\Patient;

class PatientService extends BaseService
{
    /**
     * @param Patient $model
     */
    public function __construct(Patient $model)
    {
        parent::__construct($model);
    }
}