<?php

namespace Service;

use Models\DoctorServiceModel;

class DoctorServiceModelService extends BaseService
{
    /**
     * @param DoctorServiceModel $model
     */
    public function __construct(DoctorServiceModel $model)
    {
        parent::__construct($model);
    }
}