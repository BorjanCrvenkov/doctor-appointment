<?php

namespace Service;

use Models\BaseModel;

abstract class BaseService
{
    /**
     * @param BaseModel $model
     */
    public function __construct(public BaseModel $model)
    {
    }

    /**
     * @return bool|array
     */
    public function index(): bool|array
    {
        return $this->model->all();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id): mixed
    {
        return $this->model->show($id);
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function store(array $data)
    {
        $model = $this->model->create($data);

        return $this->show($model);
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update(int $id, array $data)
    {
        $this->model->update($id, $data);

        return $this->show($id);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function destroy(int $id): bool
    {
        return $this->model->destroy($id);
    }
}