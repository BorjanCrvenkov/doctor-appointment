<?php

namespace Service;

use Models\Doctor;

class DoctorService extends BaseService
{
    /**
     * @param Doctor $model
     */
    public function __construct(Doctor $model)
    {
        parent::__construct($model);
    }
}