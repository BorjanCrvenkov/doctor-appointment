<?php

namespace Service;

use enum\AppointmentStatusEnum;
use Exception;
use Models\Appointment;
use PDO;

class AppointmentService extends BaseService
{
    /**
     * @param Appointment $model
     */
    public function __construct(Appointment $model)
    {
        parent::__construct($model);
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function update(int $id, array $data): mixed
    {
        if ($data['status'] === AppointmentStatusEnum::ACCEPTED->value) {
            $this->checkIfDoctorCanAcceptTheAppointment($id);
        }

        return parent::update($id, $data);
    }

    /**
     * @param int $id
     * @return void
     * @throws Exception
     */
    public function checkIfDoctorCanAcceptTheAppointment(int $id): void
    {
        $rez = (new Appointment())->doesOverlappingAppointmentExist($id);

        if (is_null($rez)) {
            return;
        }

        throw new Exception('You have another appointment during the new appointment');
    }
}