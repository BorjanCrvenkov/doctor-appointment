<?php

namespace Service;

use Exception;
use PDO;

class AuthenticationService
{
    /**
     * @param array $data
     * @param bool $doctorRegister
     * @return string|null
     */
    public function register(array $data, bool $doctorRegister): ?string
    {
        require 'vendor/autoload.php';

        try {
            if (!$doctorRegister) {
                $data['mobile_number'] = $_POST['mobile_number'];
            }

            require 'vendor/autoload.php';

            $service = $doctorRegister ? (new \Service\DoctorService(new \Models\Doctor())) : (new \Service\PatientService(new \Models\Patient()));

            $service->store($data);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }

        return null;
    }

    /**
     * @param string $email
     * @param string $password
     * @param bool $doctorLogin
     * @return string|void
     */
    public function login(string $email, string $password, bool $doctorLogin)
    {
        require 'vendor/autoload.php';

        $tableName = $doctorLogin ? 'doctors' : 'patients';

        try {
            $pdo = new PDO('mysql:host=localhost;dbname=mydb', 'root', 'password');

            $query = "SELECT * FROM {$tableName} WHERE email = :email and password = :password limit 1";
            $statement = $pdo->prepare($query);
            $result = $statement->execute(['email' => $email, 'password' => $password]);

            if ($result) {
                $row = $statement->fetch();
                if ($row) {
                    session_start();
                    setcookie('user_id', $row['id'], time() + 60 * 60 * 60);
                    $_SESSION['user_id'] = $row['id'];

                    $redirectPath = $doctorLogin ? '/doctor' : '/patient';

                    header("Location: {$redirectPath}");
                    exit;
                } else {
                    $loginError = 'Invalid credentials';
                }
            } else {
                $loginError = "Query execution failed.";
            }
        } catch (Exception $e) {
            echo 'exception ' . $e->getMessage();
        }

        return $loginError ?? '';
    }
}