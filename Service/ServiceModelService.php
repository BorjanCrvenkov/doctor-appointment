<?php

namespace Service;

use Models\ServiceModel;

class ServiceModelService extends BaseService
{
    /**
     * @param ServiceModel $model
     */
    public function __construct(ServiceModel $model)
    {
        parent::__construct($model);
    }
}