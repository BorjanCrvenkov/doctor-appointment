<?php

use Exception;

class PatientController
{
    /**
     * @return void
     */
    public function mainPage(): void
    {
        require_once('views/patient/patient-main-page.php');
    }

    /**
     * @return string
     */
    public function scheduleAppointment(): string
    {
        require 'vendor/autoload.php';

        $createData = [
            'time_start'           => $_POST['time_start'],
            'time_end'             => $_POST['time_end'],
            'special_requirements' => $_POST['special_requirements'],
            'medical_conditions'   => $_POST['medical_conditions'],
            'service_id'           => $_POST['service_id'],
            'doctor_id'            => $_POST['doctor_id'],
            'patient_id'           => (int)$_COOKIE['user_id'],
            'status'               => \enum\AppointmentStatusEnum::PENDING->value
        ];

        try {
            (new \Service\AppointmentService(new \Models\Appointment()))->store($createData);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }

        header("Refresh:0");
        return '';
    }

    /**
     * @return string
     */
    public function cancelAppointment(): string
    {
        $appointmentId = (int)$_POST['appointment_id'];

        $updateData = [
            'status' => \enum\AppointmentStatusEnum::CANCELED->value
        ];

        try {
            (new \Service\AppointmentService(new \Models\Appointment()))->update($appointmentId, $updateData);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }

        return '';
    }

    /**
     * @return string
     */
    public function rescheduleAppointment(): string
    {
        require 'vendor/autoload.php';

        $appointmentId = (int)$_POST['appointment_id'];

        $createData = [
            'time_start' => $_POST['time_start'],
            'time_end'   => $_POST['time_end'],
            'status'     => \enum\AppointmentStatusEnum::PENDING->value
        ];

        try {
            (new \Service\AppointmentService(new \Models\Appointment()))->update($appointmentId, $createData);
        } catch (Exception $exception) {
            return $exception->getMessage();
        }
        header("Refresh:0");
        return '';
    }
}