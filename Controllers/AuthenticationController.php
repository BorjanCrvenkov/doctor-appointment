<?php

class AuthenticationController
{
    /**
     * @return void
     */
    public function showRegisterForm(): void
    {
        require("views/register-form.php");
    }

    /**
     * @return string
     */
    public function register(): string
    {
        require 'vendor/autoload.php';

        $data = [
            'first_name' => $_POST['first_name'],
            'last_name'  => $_POST['last_name'],
            'email'      => $_POST['email'],
            'password'   => $_POST['password'],
        ];
        $doctorRegister = !is_null($_POST['doctor_register']);

        $message = (new \Service\AuthenticationService())->register($data, $doctorRegister);

        if (is_null($message)) {
            echo '<script>window.location.href = "/login";</script>';
            exit;
        }

        return $message;
    }

    /**
     * @return void
     */
    public function showLoginForm(): void
    {
        require_once("views/login-form.php");
    }

    /**
     * @return string|null
     */
    public function login(): ?string
    {
        require 'vendor/autoload.php';

        $email = $_POST['email'];
        $password = $_POST['password'];
        $doctorLogin = !is_null($_POST['doctor_login']);

        return (new \Service\AuthenticationService())->login($email, $password, $doctorLogin);
    }

    /**
     * @return void
     */
    public function logout(): void
    {
        session_destroy();
        header("Location: /login");
        exit;
    }
}
?>