<!DOCTYPE html>
<html>
<head>
    <title>Register Form</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">

</head>
<body>
<?php

require('header.php')
?>
<div class="container mt-5">
    <h2>Register</h2>
    <ul class="nav nav-tabs">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#tabs-1">Register as Doctor</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#tabs-2">Register as Patient</a>
        </li>
    </ul>
    <div class="tab-content">
        <div id="tabs-1" class="tab-pane fade show active">
            <div class="mt-3">
                <form method="post">
                    <div class="form-group">
                        <label for="first_name">First name:</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" required>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last name:</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" required>
                    </div>
                    <div class="form-group">
                        <label for="doctor-email">Email:</label>
                        <input type="email" class="form-control" id="doctor-email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="doctor-password">Password:</label>
                        <input type="password" class="form-control" id="doctor-password" name="password" required>
                    </div>
                    <input type="hidden" name="doctor_register" value="true">
                    <button type="submit" name="doctor-register" class="btn btn-primary">Register as Doctor</button>
                </form>
            </div>
        </div>
        <div id="tabs-2" class="tab-pane fade">
            <div class="mt-3">
                <form method="post">
                    <div class="form-group">
                        <label for="first_name">First name:</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" required>
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last name:</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" required>
                    </div>
                    <div class="form-group">
                        <label for="patient-email">Email:</label>
                        <input type="email" class="form-control" id="patient-email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="patient-password">Password:</label>
                        <input type="password" class="form-control" id="patient-password" name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="mobile_number">Mobile number:</label>
                        <input type="text" class="form-control" id="mobile_number" name="mobile_number" required>
                    </div>
                    <button type="submit" name="patient-register" class="btn btn-primary">Register as Patient</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>
</html>


<?php
if ($_REQUEST) {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
        return;
    }

    $authenticationController = new AuthenticationController();

    $registerError = $authenticationController->register();
    echo "<p style='color: red;'>$registerError</p>";
}
?>
