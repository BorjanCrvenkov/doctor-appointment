<div>
    <?php
    require 'vendor/autoload.php';

    $canceledAppointments = (new \Models\Appointment())->filterByPatientIdAndStatus((int)$_COOKIE['user_id'], \enum\AppointmentStatusEnum::CANCELED->value);
    foreach ($canceledAppointments as $appointment) {
        $services = (new \Models\ServiceModel())->filterByDoctorId($appointment['doctor_id']);
        ?>
        <div class="border p-4 mb-2">
            <p>Time start: <?= $appointment['time_start'] ?></p>
            <p>Time end: <?= $appointment['time_end'] ?></p>
            <p>Medical conditions: <?= $appointment['medical_conditions'] ?? '/' ?></p>
            <p>Special requirements: <?= $appointment['special_requirements'] ?? '/' ?></p>
        </div>
        <?php
    }
    ?>
</div>
