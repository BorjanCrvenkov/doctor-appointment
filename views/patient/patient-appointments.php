<h3>Appointments</h3>
<div id="tabs">
    <ul>
        <li><a href="#tabs-1">Accepted</a></li>
        <li><a href="#tabs-2">Pending</a></li>
        <li><a href="#tabs-3">Canceled</a></li>
    </ul>
    <div id="tabs-1">
        <?php
        require('patient-accepted-appointments.php');
        ?>
    </div>
    <div id="tabs-2">
        <?php
        require('patient-pending-appointments.php');
        ?>
    </div>
    <div id="tabs-3">
        <?php
        require('patient-canceled-appointments.php');
        ?>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>

