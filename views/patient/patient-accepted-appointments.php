<div>
    <?php
    require 'vendor/autoload.php';
    $templatePath = 'patient-display-appointment.php';

    $acceptedAppointments = (new \Models\Appointment())->filterByPatientIdAndStatus((int)$_COOKIE['user_id'], \enum\AppointmentStatusEnum::ACCEPTED->value);
    foreach ($acceptedAppointments as $appointment) {
        $services = (new \Models\ServiceModel())->filterByDoctorId($appointment['doctor_id']);
        include($templatePath);
    }
    ?>
</div>

<?php
if ($_REQUEST) {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['action'])) {
        return;
    }

    $action = $_POST['action'];

    if ($action == 'cancel' || $action == 'reschedule' || $action == 'accept') {
        $patientController = new PatientController();

        if ($action == 'cancel') {
            $loginError = $patientController->cancelAppointment();
        } elseif ($action == 'reschedule') {
            $loginError = $patientController->rescheduleAppointment();
        }

        echo "<p style='color: red;'>$loginError</p>";
    }
}
?>
