<div class="border mb-2 p-4">
    <h4>Doctor: <?= $doctor['first_name'] ?> <?= $doctor['last_name'] ?></h4>
    <button id="modal-btn" name="<?= $doctor['id'] ?>" onclick="openModal(event)" class="btn btn-primary">Schedule
        Appointment
    </button>
</div>

<div id="<?= $doctor['id'] ?>" class="modal">
    <div class="modal-content container">
        <h4 class="modal-title">Appointment with <?= $doctor['first_name'] ?> <?= $doctor['last_name'] ?></h4>
        <form method="post">
            <div class="form-group">
                <label for="time_start">Time start</label>
                <input type="datetime-local" class="form-control" name="time_start" required>
            </div>
            <div class="form-group">
                <label for="time_end">Time end</label>
                <input type="datetime-local" class="form-control" name="time_end" required>
            </div>
            <div class="form-group">
                <label for="special_requirements">Special requirements</label>
                <input type="text" class="form-control" name="special_requirements">
            </div>
            <div class="form-group">
                <label for="medical_conditions">Medical conditions</label>
                <input type="text" class="form-control" name="medical_conditions">
            </div>
            <div class="form-group">
                <label for="service">Service</label>
                <select class="form-control" name="service_id" required>
                    <?php foreach ($services as $service): ?>
                        <option value="<?= $service['id'] ?>"><?= $service['name'] ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <input type="hidden" name="action" value="schedule">
            <input type="hidden" name="doctor_id" value="<?= $doctor['id'] ?>">
            <button type="submit" class="btn btn-primary">Schedule appointment</button>
            <button class="btn btn-secondary" name="<?= $doctor['id'] ?>" onclick="closeModal(event)">Close</button>
        </form>
    </div>

</div>