<!DOCTYPE html>
<html>
<head>
    <title>Patient Main Page</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#tabs").tabs();
        });
        $(function () {
            $("#dialog").dialog();
        });

        let spans = document.getElementsByClassName("close");

        let openedModal;

        function openModal(event) {
            let modal = document.getElementById(event.target.name);
            modal.style.display = "block";
            openedModal = modal;
        }

        function closeModal() {
            openedModal.style.display = "none";
        }
    </script>
    <link rel="stylesheet" href="../../resources/modal-styles.css">
</head>
<?php

require('./views/header.php')
?>
<body>
<h2>Patient Main Page</h2>
<div class="container">
    <h3>Doctors</h3>
    <?php
    require 'vendor/autoload.php';
    $patientTemplatePath = 'patient-display-doctor.php';

    $doctors = (new \Models\Doctor())->all();

    foreach ($doctors as $doctor) {
        $services = (new \Models\ServiceModel())->filterByDoctorId($doctor['id']);
        include($patientTemplatePath);
    }

    require('patient-appointments.php');

    if ($_REQUEST) {
        if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['action'])) {
            return;
        }

        $action = $_POST['action'];

        if ($action == 'schedule') {
            $patientController = new PatientController();

            $loginError = $patientController->scheduleAppointment();
            echo "<p style='color: red;'>$loginError</p>";
        }
    }
    ?>
</div>
</body>
</html>
<script>
    const timeStartInputs = document.querySelectorAll('input[name="time_start"]');
    const timeEndInputs = document.querySelectorAll('input[name="time_end"]');

    const now = new Date();
    const currentYear = now.getFullYear();
    const currentMonth = now.getMonth() + 1;
    const currentDay = now.getDate();

    const minTime = new Date(`${currentYear}-${currentMonth.toString().padStart(2, '0')}-${currentDay.toString().padStart(2, '0')}T09:00`);

    const maxTime = new Date('9999-12-31T23:59:59');

    timeStartInputs.forEach(input => {
        input.setAttribute('min', minTime.toISOString().slice(0, -8));
        input.setAttribute('max', maxTime.toISOString().slice(0, -8));
    });

    timeEndInputs.forEach(input => {
        input.setAttribute('min', minTime.toISOString().slice(0, -8));
        input.setAttribute('max', maxTime.toISOString().slice(0, -8));
    });
</script>