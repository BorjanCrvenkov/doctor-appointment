<div class="container mt-5">
    <h3>Appointments</h3>
    <div id="tabs">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#tabs-1">Accepted</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tabs-2">Pending</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#tabs-3">Canceled</a>
            </li>
        </ul>
        <div class="tab-content">
            <div id="tabs-1" class="tab-pane fade show active">
                <?php require('doctor-accepted-appointments.php'); ?>
            </div>
            <div id="tabs-2" class="tab-pane fade">
                <?php require('doctor-pending-appointments.php'); ?>
            </div>
            <div id="tabs-3" class="tab-pane fade">
                <?php require('doctor-canceled-appointments.php'); ?>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
</body>
</html>