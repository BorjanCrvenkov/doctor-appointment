<div class="border p-4 mb-2">
    <p>Time start: <?= $appointment['time_start'] ?></p>
    <p>Time end: <?= $appointment['time_end'] ?></p>
    <p>Medical conditions: <?= $appointment['medical_conditions'] ?? '/' ?></p>
    <p>Special requirements: <?= $appointment['special_requirements'] ?? '/' ?></p>
    <div class="row" style="margin-left: 70px">
        <?php if ($appointment['time_start'] >= date("Y-m-d H:i:s")): ?>
            <div class="col-4">
                <form method='post'>
                    <input type='hidden' name='action' value='cancel'>
                    <input hidden='hidden' name='appointment_id' value='<?= $appointment['id'] ?>'>
                    <button type='submit' class="btn btn-danger">Cancel appointment</button>
                </form>
            </div>
            <div class="col-4">
                <button id='modal-btn' name="<?= $appointment['id'] ?>" onclick='openModal(event)'
                        class="btn btn-primary">Reschedule Appointment
                </button>
            </div>
            <?php if ($appointment['status'] == \enum\AppointmentStatusEnum::PENDING->value): ?>
                <div class="col-4">
                    <form method='post'>
                        <input type='hidden' name='action' value='accept'>
                        <input hidden='hidden' name='appointment_id' value='<?= $appointment['id'] ?>'>
                        <button type='submit' class="btn btn-success">Accept appointment</button>
                    </form>
                </div>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
<div id="<?= $appointment['id'] ?>" class="modal">
    <div class="modal-content container">
        <form method="post">
            <div class="form-group">
                <label for="time_start">Time start</label>
                <input type="datetime-local" class="form-control" name="time_start"
                       value='<?= $appointment['time_start'] ?>' required>
            </div>
            <div class="form-group">
                <label for="time_end">Time end</label>
                <input type="datetime-local" class="form-control" name="time_end"
                       value='<?= $appointment['time_end'] ?>' required>
            </div>
            <input type="hidden" name="action" value="reschedule">
            <input type="hidden" name="appointment_id" value="<?= $appointment['id'] ?>">
            <button type="submit" class="btn btn-primary">Reschedule appointment</button>
            <button class="btn btn-secondary" name="<?= $appointment['id'] ?>" onclick="closeModal(event)">Close
            </button>
        </form>
    </div>

</div>