<div>
    <?php
    require 'vendor/autoload.php';
    $templatePath = 'doctor-display-appointment.php';

    $acceptedAppointments = (new \Models\Appointment())->filterByDoctorIdAndStatus((int)$_COOKIE['user_id'], \enum\AppointmentStatusEnum::ACCEPTED->value);
    foreach ($acceptedAppointments as $appointment) {
        $services = (new \Models\ServiceModel())->filterByDoctorId($appointment['doctor_id']);
        include($templatePath);
    }
    ?>
</div>

<?php
if ($_REQUEST) {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['action'])) {
        return;
    }

    $action = $_POST['action'];

    if ($action == 'cancel' || $action == 'reschedule' || $action == 'accept') {
        $doctorController = new DoctorController();

        if ($action == 'cancel') {
            $loginError = $doctorController->cancelAppointment();
        } elseif ($action == 'reschedule') {
            $loginError = $doctorController->rescheduleAppointment();
        }

        echo "<p style='color: red;'>$loginError</p>";
    }
}
?>
