<div>
    <?php
    require 'vendor/autoload.php';
    $templatePath = 'doctor-display-appointment.php';

    $pendingAppointments = (new \Models\Appointment())->filterByDoctorIdAndStatus((int)$_COOKIE['user_id'], \enum\AppointmentStatusEnum::PENDING->value);
    foreach ($pendingAppointments as $appointment) {
        include($templatePath);
    }
    ?>
</div>

<?php
if ($_REQUEST) {
    if ($_SERVER['REQUEST_METHOD'] !== 'POST' || !isset($_POST['action'])) {
        return;
    }

    $action = $_POST['action'];

    if ($action == 'cancel' || $action == 'reschedule' || $action == 'accept') {
        $doctorController = new DoctorController();

        if ($action == 'cancel') {
            $loginError = $doctorController->cancelAppointment();
        } elseif ($action == 'reschedule') {
            $loginError = $doctorController->rescheduleAppointment();
        } elseif ($action == 'accept') {
            $loginError = $doctorController->acceptAppointment();
        }

        echo "<p style='color: red;'>$loginError</p>";
    }
}
?>
