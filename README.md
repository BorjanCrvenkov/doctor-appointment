# Doctor Appointment



## Starting the project
Run the following commands <br />
composer dump-autoload <br />
php -S localhost:8080 <br />

## Database tables

### Doctors
CREATE TABLE `doctors` ( <br />
  `id` int NOT NULL AUTO_INCREMENT, <br />
  `first_name` varchar(45) DEFAULT NULL, <br />
  `last_name` varchar(45) DEFAULT NULL, <br />
  `email` varchar(45) DEFAULT NULL, <br />
  `password` varchar(45) DEFAULT NULL, <br />
  PRIMARY KEY (`id`), <br />
  UNIQUE KEY `email_UNIQUE` (`email`) <br />
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci <br />

### Patients
CREATE TABLE `patients` ( <br />
  `id` int NOT NULL AUTO_INCREMENT, <br />
  `email` varchar(255) NOT NULL, <br />
  `password` varchar(255) NOT NULL, <br />
  `mobile_number` varchar(255) NOT NULL, <br />
  `first_name` varchar(45) NOT NULL, <br />
  `last_name` varchar(45) NOT NULL, <br />
  PRIMARY KEY (`id`), <br />
  UNIQUE KEY `mobile_number_UNIQUE` (`mobile_number`), <br />
  UNIQUE KEY `email_UNIQUE` (`email`) <br />
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci <br />

### Appointments
CREATE TABLE `appointments` ( <br />
  `id` int NOT NULL AUTO_INCREMENT, <br />
  `time_start` timestamp NOT NULL, <br />
  `time_end` timestamp NOT NULL, <br />
  `special_requirements` text, <br />
  `medical_conditions` text, <br />
  `status` varchar(255) NOT NULL, <br />
  `patient_id` int NOT NULL, <br />
  `doctor_id` int NOT NULL, <br />
  `service_id` int NOT NULL, <br />
  PRIMARY KEY (`id`), <br />
  KEY `appointments_ibfk_1` (`patient_id`), <br />
  KEY `appointments_ibfk_2` (`doctor_id`), <br />
  KEY `appointments_ibfk_3` (`service_id`), <br />
  CONSTRAINT `appointments_ibfk_1` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, <br />
  CONSTRAINT `appointments_ibfk_2` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE, <br />
  CONSTRAINT `appointments_ibfk_3` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE <br />
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci <br />

### Services
CREATE TABLE `services` ( <br />
  `id` int NOT NULL AUTO_INCREMENT, <br />
  `name` varchar(255) DEFAULT NULL, <br />
  PRIMARY KEY (`id`) <br />
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci <br />

### Doctor Services
CREATE TABLE `doctor_services` ( <br />
  `id` int NOT NULL AUTO_INCREMENT, <br />
  `doctor_id` int NOT NULL, <br />
  `service_id` int NOT NULL, <br />
  PRIMARY KEY (`id`), <br />
  KEY `doctor_id` (`doctor_id`), <br />
  KEY `service_id` (`service_id`), <br />
  CONSTRAINT `doctor_services_ibfk_1` FOREIGN KEY (`doctor_id`) REFERENCES `doctors` (`id`), <br />
  CONSTRAINT `doctor_services_ibfk_2` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) <br />
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci <br />
