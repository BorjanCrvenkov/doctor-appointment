<?php

namespace Models;

use PDO;
use PDOException;

class BaseModel
{
    public static PDO $pdo;
    public static string $tableName;

    /**
     * @param string $tableName
     */
    public function __construct(string $tableName)
    {
        self::$tableName = $tableName;
        try {
            $host = "localhost";
            $username = "root";
            $password = "password";
            $database = 'mydb';

            $pdo = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $username, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            self::$pdo = $pdo;
        } catch (PDOException $e) {
            die("Connection failed: " . $e->getMessage());
        }
    }

    /**
     * @return bool|array
     */
    public static function all(): bool|array
    {
        $tableName = self::$tableName;
        $query = "SELECT * FROM {$tableName}";
        $stmt = self::$pdo->query($query);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $id
     * @return mixed
     */
    public static function show(int $id): mixed
    {
        $tableName = self::$tableName;
        $query = "SELECT * FROM {$tableName} WHERE id = :id";
        $stmt = self::$pdo->prepare($query);
        $stmt->execute(['id' => $id]);

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param array $data
     * @return bool
     */
    public static function create(array $data): bool
    {
        $tableName = self::$tableName;
        $fields = implode(', ', array_keys($data));
        $placeholders = ':' . implode(', :', array_keys($data));

        $query = "INSERT INTO {$tableName} ({$fields}) VALUES ({$placeholders})";
        $stmt = self::$pdo->prepare($query);
        $stmt->execute($data);

        return true;
    }

    /**
     * @param int $id
     * @param array $data
     * @return bool
     */
    public static function update(int $id, array $data): bool
    {
        $tableName = self::$tableName;
        $updateFields = [];

        foreach ($data as $key => $value) {
            $updateFields[] = "{$key} = :{$key}";
        }

        $updateFields = implode(', ', $updateFields);

        $query = "UPDATE {$tableName} SET {$updateFields} WHERE id = :id";
        $data['id'] = $id;
        $stmt = self::$pdo->prepare($query);
        $stmt->execute($data);

        return true;
    }

    /**
     * @param int $id
     * @return bool
     */
    public static function destroy(int $id): bool
    {
        $tableName = self::$tableName;
        $query = "DELETE FROM {$tableName} WHERE id = :id";
        $stmt = self::$pdo->prepare($query);
        $stmt->execute(['id' => $id]);

        return true;
    }
}