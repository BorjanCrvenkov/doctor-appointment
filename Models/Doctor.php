<?php

namespace Models;

class Doctor extends BaseModel
{
    /**
     * @param int $id
     * @param string $firstName
     * @param string $lastName
     * @param string $email
     * @param string $password
     */
    public function __construct(
        public int    $id = 0,
        public string $firstName = '',
        public string $lastName = '',
        public string $email = '',
        public string $password = '',
    )
    {
        parent::__construct('doctors');
    }
}