<?php

namespace Models;

use PDO;

class ServiceModel extends BaseModel
{
    /**
     * @param int $id
     * @param string $name
     */
    public function __construct(
        public int    $id = 0,
        public string $name = '',
    )
    {
        parent::__construct('services');
    }

    /**
     * @param int $doctorId
     * @return bool|array
     */
    public function filterByDoctorId(int $doctorId): bool|array
    {
        $tableName = self::$tableName;
        $query = "SELECT * FROM {$tableName} JOIN doctor_services ON services.id = service_id WHERE doctor_id = {$doctorId}";
        $stmt = self::$pdo->query($query);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

}