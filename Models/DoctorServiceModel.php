<?php

namespace Models;

class DoctorServiceModel extends BaseModel
{
    /**
     * @param int $id
     * @param int $doctorId
     * @param int $serviceId
     */
    public function __construct(
        public int $id,
        public int $doctorId,
        public int $serviceId,
    )
    {
        parent::__construct('doctor_services');
    }
}