<?php

namespace Models;

use enum\AppointmentStatusEnum;
use PDO;

class Appointment extends BaseModel
{
    /**
     * @param int $id
     * @param $timeStart
     * @param $timeEnd
     * @param string $specialRequirements
     * @param string $medicalConditions
     * @param string $status
     * @param int $patientId
     * @param int $doctorId
     * @param int $serviceId
     */
    public function __construct(
        public int    $id = 0,
        public        $timeStart = '',
        public        $timeEnd = '',
        public string $specialRequirements = '',
        public string $medicalConditions = '',
        public string $status = '',
        public int    $patientId = 0,
        public int    $doctorId = 0,
        public int    $serviceId = 0,
    )
    {
        parent::__construct('appointments');
    }

    /**
     * @param int $patientId
     * @param string $status
     * @return bool|array
     */
    public function filterByPatientIdAndStatus(int $patientId, string $status): bool|array
    {
        $tableName = self::$tableName;
        $query = "SELECT * FROM {$tableName} where patient_id = {$patientId} AND `status` = '{$status}'";
        $stmt = self::$pdo->query($query);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $doctorId
     * @param string $status
     * @return bool|array
     */
    public function filterByDoctorIdAndStatus(int $doctorId, string $status): bool|array
    {
        $tableName = self::$tableName;
        $query = "SELECT * FROM {$tableName} where doctor_id = {$doctorId} AND `status` = '{$status}'";
        $stmt = self::$pdo->query($query);

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param int $appointmentId
     * @return mixed
     */
    public function doesOverlappingAppointmentExist(int $appointmentId): mixed
    {
        $appointment = $this->show($appointmentId);

        $params = [
            'doctorId'             => $appointment['doctor_id'],
            'status'               => AppointmentStatusEnum::ACCEPTED->value,
            'appointmentTimeStart' => $appointment['time_start'],
            'appointmentTimeEnd'   => $appointment['time_end'],
            'appointmentId'        => $appointmentId
        ];

        $query = "SELECT 1 FROM appointments 
         WHERE doctor_id = :doctorId AND status = :status 
          AND (
              (:appointmentTimeStart BETWEEN time_start AND time_end)
              OR
              (:appointmentTimeEnd BETWEEN time_start AND time_end)
          )
          AND id != :appointmentId
          LIMIT 1";
        $stmt = self::$pdo->prepare($query);
        $stmt->execute($params);

        $rez = $stmt->fetch(PDO::FETCH_ASSOC);

        return $rez [0];
    }
}