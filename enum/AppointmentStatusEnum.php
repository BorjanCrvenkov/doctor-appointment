<?php

namespace enum;

enum AppointmentStatusEnum: string
{
    case PENDING = 'pending';
    case ACCEPTED = 'accepted';
    case CANCELED = 'canceled';
}
