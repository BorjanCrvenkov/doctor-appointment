<?php

$routes = [
    '/register' => 'AuthenticationController@showRegisterForm',
    '/login'    => 'AuthenticationController@showLoginForm',
    '/logout'   => 'AuthenticationController@logout',
    '/doctor'   => 'DoctorController@mainPage',
    '/patient'  => 'PatientController@mainPage',
];

$requestUrl = $_SERVER['REQUEST_URI'];

if (array_key_exists($requestUrl, $routes)) {
    $controllerAction = $routes[$requestUrl];
    list($controllerName, $actionName) = explode('@', $controllerAction);

    require_once("Controllers/$controllerName.php");
    $controller = new $controllerName();
    $controller->$actionName();
} else {
    header("HTTP/1.0 404 Not Found");
    echo "404 Not Found";
}
?>